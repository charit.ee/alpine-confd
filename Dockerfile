FROM golang:1.6-alpine
MAINTAINER Vasili Sviridov <vasili@charit.ee>
VOLUME /etc/confd/
ENV CONFD_VERSION=master
RUN apk --no-cache --update add git \
    && go get --tags $CONFD_VERSION -u github.com/kelseyhightower/confd \
    && apk del git \
    && rm -rf /var/cache/apk/*
CMD ["confd"]
